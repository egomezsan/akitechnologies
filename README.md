# AkiTechnologies

**_Eduardo Gomez Sanchez_**

Take home project
- Spring application *boot allowed
- Should expose a GET endpoint `/_status` that returns `OK` // with status 200
- Should expose an endpoint `/service`
  - Receives json, possible payloads:
    `{"op": "A", values: [int, ..., ..]}` // any number of ints
    `{"op": "B", values: [int, int]|[int]}` // when there's two values, the first int must be larger than the second
    `{"op": "C", values: [int]}` // single int
  - Returns '{status: "processing"}' // with status 200
    - When values doesn't match the expected number of values, returns: `{error: "Invalid number of operands"}` // with status 400
    - When there's an internal server error, returns: `{error: "Oops, there was a problem!"}` // with status 500
    - Don't return plain strings, use serialization (jackson, gson, ...)
    - Use `org.springframework.validation.Validator` for validation errors
  - Internally it should:
    - Randomly produce a RuntimeException (Inject a dependency that will behave this way when invoked)
    - Call an asynchronous process that takes long (Thread.sleep(200000)) that:
      - Randomly produces a RuntimeException (Inject a dependency that will behave this way when invoked)
      - Log when the random error occurs
    - Return after invoking the async process
  - Using aspects (spring flavor) on the "/service" endpoint:
    - When ok do GET on http service at:
      - `{SCHEME}://{HOST}:{PORT}/done`
    - When error do POST on http service at:
      - `{SCHEME}://{HOST}:{PORT}/error`
      - With a json body as follows `{"error": "#{exception.getMessage()}", "timestamp": #{System.currentTimeMillis()}}`
    - NOTE: SCHEME, HOST and PORT should be defined as environment variables
- Tests:
  - Unit test the validator
  - Integration Test for the `/service` endpoint
  - Integration Test for the aspect
- Build:
  - Use maven
  - Execute surefire and failsafe plugins during build
  - Generate executable jar
- Other:
  - Use constructor injection
  - Use consistent styling
  - Use some mocking library for UTs
  - Use Java 8
  
  ##Environment variables and default values:
  
  - aki_technologies_scheme = http
  - aki_technologies_host = localhost
  - aki_technologies_port = 8080
  - aki.technologies_async_idle= 200000
 
  
  