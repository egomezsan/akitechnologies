package com.aki.technologies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.aki.technologies.configuration.AkiConfiguration;

/**
 * Main Application start point
 * 
 * @author eduardo.gomez
 *
 */
@SpringBootApplication
@Import(value=AkiConfiguration.class)
public class AkiTechnologiesProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AkiTechnologiesProjectApplication.class, args);
	}
}
