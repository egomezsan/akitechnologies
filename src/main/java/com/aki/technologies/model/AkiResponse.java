package com.aki.technologies.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Aki Success response
 * @author eduardo.gomez
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class AkiResponse implements Serializable{

	private static final long serialVersionUID = -5751735330503041347L;
	
	private String status;

}
