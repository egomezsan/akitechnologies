package com.aki.technologies.model;

import java.io.Serializable;
import java.util.List;
import com.aki.technologies.validation.OptionValuesConstraint;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Aki Request object
 * @author eduardo.gomez
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
@OptionValuesConstraint(optionField="option",valuesField="values")
public class AkiRequest implements Serializable{
	
	private static final long serialVersionUID = 465835663706094516L;

	@JsonProperty("op")
	private String option;
	
	private List<Integer> values;

}
