package com.aki.technologies.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Aki error response
 * @author eduardo.gomez
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class ErrorResponse implements Serializable{

	private static final long serialVersionUID = -7291484483170519055L;
	
	private String error;
	private Long timestamp;
	 
}
