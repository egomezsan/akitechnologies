package com.aki.technologies.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import static com.aki.technologies.util.ServiceConstants.*;

/**
 * Wraps all environment variables
 * 
 * @author eduardo.gomez
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Component
public final class AkiSettings {
	
	@Value(ENV_NAME_SCHEME)
	private String scheme;
	
	@Value(ENV_NAME_HOST)
	private String host;
	
	@Value(ENV_NAME_PORT)
	private String port;
	
	@Value(ENV_NAME_ASYNC_IDLE)
	private Long asyncIdleTime;

}
