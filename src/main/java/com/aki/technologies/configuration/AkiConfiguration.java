package com.aki.technologies.configuration;

import java.util.Random;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Aki Application specific configuration and bean definition class
 * 
 * @author eduardo.gomez
 *
 */
@Configuration
@EnableAsync
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class AkiConfiguration {
	
	/**
	 * Random object to be used for generating random booleans
	 * @return Random
	 */
	@Bean
	public Random getRandom() 
	{
		return new Random();
	}
	
	/**
	 * Object mapper user bean used for serialization and deserialization (ignores nulls fields)
	 * @return ObjectMapper
	 */
	@Bean
    public ObjectMapper objectMapper() {
		
		ObjectMapper mapper = new ObjectMapper();
	    mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper;
    }

}
