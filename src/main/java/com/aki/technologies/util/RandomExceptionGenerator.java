package com.aki.technologies.util;

import java.util.Random;
import org.springframework.stereotype.Component;
import com.aki.technologies.exception.BaseException;
import lombok.AllArgsConstructor;
import static com.aki.technologies.util.ServiceConstants.*;

/**
 * Utility class which is injected on service layer 
 * and when invoqued it will produce a runtime exception
 * @author eduardo.gomez
 *
 */
@AllArgsConstructor
@Component
public class RandomExceptionGenerator {
	
	private Random random;
	
	/**
	 * When invoqued it will produce a runtime exception
	 */
	public void produceRandomRuntimeException() {
		if (random.nextBoolean()) {
			throw new BaseException(RANDOM_EXCEPTION_MESSAGE);
		}
	}

}
