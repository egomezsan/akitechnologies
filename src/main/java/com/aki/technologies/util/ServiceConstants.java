package com.aki.technologies.util;

/**
 * Specific Service Constants values
 * 
 * @author eduardo.gomez
 *
 */
public class ServiceConstants {
	
	/**
	 * Prevent instance creation since only constant fields are used here
	 */
	private ServiceConstants() {
		
	}
	
	// Custom Settings Environment Variables names
	public static final String ENV_NAME_SCHEME = "${aki.technologies.scheme}";
	public static final String ENV_NAME_HOST = "${aki.technologies.host}";
	public static final String ENV_NAME_PORT = "${aki.technologies.port}";
	public static final String ENV_NAME_ASYNC_IDLE = "${aki.technologies.async.idle}";
	
	// Generic constants
	public static final String POST_ANNOTATION_POINTCUT = "@annotation(org.springframework.web.bind.annotation.PostMapping)";
	public static final String ADVICE_POST_ANNOTATION_POINTCUT = "(postMapping())";
	public static final String HEALTH_CHECK_MAPPING = "/_status";
	public static final String SERVICE_MAPPING = "/service";
	public static final String SUCCESS_RESPONSE_MESSAGE = "processing";
	public static final String ERROR_RESPONSE_MESSAGE = "Oops, there was a problem!";
	public static final String URL_STRING_FORMAT = "%s://%s:%s";
	public static final String RANDOM_EXCEPTION_MESSAGE = "Random exception happenned!";
	public static final String VALIDATION_ERROR_MESSAGE = "Invalid number of operands";
	public static final String POST_ERROR_SUCCESS_MESSAGE = "Error Posted Successfully";
	public static final String ERROR_ENDPOINT_URI = "/error";
	public static final String DONE_ENDPOINT_URI = "/done";
	

}
