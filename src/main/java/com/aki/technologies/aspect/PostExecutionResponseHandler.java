package com.aki.technologies.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import com.aki.technologies.model.AkiResponse;
import com.aki.technologies.service.IAkiService;
import lombok.AllArgsConstructor;
import static com.aki.technologies.util.ServiceConstants.*;

/**
 * Aspect class for handling success or error responses on POST calls
 * 
 * @author eduardo.gomez
 *
 */
@AllArgsConstructor
@Aspect
@Component
public class PostExecutionResponseHandler {
	
	private IAkiService service;
	
	/**
	 * Define pointcut on annotation @PostMapping
	 */
	@Pointcut(POST_ANNOTATION_POINTCUT)
    public void postMapping() {}
    
	/**
	 * After Returning advice for handling success responses. It will call GET on configured endpoint
	 * 
	 * @param jp <b>Joinpoint</b> containing the request as args
	 * @param response <b>AkiResponse</b> representing the success response
	 */
	@AfterReturning(value= ADVICE_POST_ANNOTATION_POINTCUT,returning = "response")
    public void forwardResults(JoinPoint jp, AkiResponse response) {
		
		// Call get on http service
		service.getDone();
    }
	
	/**
	 * After Throwing advice for handling error responses. It will call POST on configured endpoint
	 * 
	 * @param jp <b>Joinpoint</b> containing the request as args
	 * @param ex Throwable exception to be forwarded to service
	 */
	@AfterThrowing(value= ADVICE_POST_ANNOTATION_POINTCUT,throwing = "ex")
    public void doRecoveryActions(JoinPoint jp, Throwable ex) {
		// Call Post on http service
		service.postError(ex);
	}

}
