package com.aki.technologies.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.aki.technologies.configuration.AkiSettings;
import com.aki.technologies.exception.BaseException;
import com.aki.technologies.model.ErrorResponse;
import com.aki.technologies.util.RandomExceptionGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.aki.technologies.util.ServiceConstants.*;

/**
 * Service implementation
 * @author eduardo.gomez
 *
 */
@Service
public class AkiService implements IAkiService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private RestTemplate restTemplate;
	private final String baseUrl;
	private AkiSettings settings;

	public AkiService(RestTemplateBuilder restTemplateBuilder, RandomExceptionGenerator exceptionGenerator, AkiSettings settings, ObjectMapper mapper) {
		this.restTemplate = restTemplateBuilder.build();
		this.exceptionGenerator = exceptionGenerator;
		this.baseUrl = String.format(URL_STRING_FORMAT, settings.getScheme(),settings.getHost(),settings.getPort());
		this.mapper = mapper;
		this.settings = settings;
	}
	
	@Override
	public void setRestTemplate(RestTemplate restTemplate)
	{
		this.restTemplate = restTemplate;
	}
	
	private RandomExceptionGenerator exceptionGenerator;
	private ObjectMapper mapper;

	/**
	 * Produces a runtime excpetion randomly
	 */
	@Override
	public void produceRandomRuntimeException() {
		exceptionGenerator.produceRandomRuntimeException();
	}

	/**
	 * Produces a runtime excpetion randomly async, waiting 200s
	 */
	@Async
	@Override
	public void produceRandomRuntimeExceptionAsync() {

		logger.info("Started execution of async method");

		try {
			// Stay idle for configured time
			Thread.sleep(settings.getAsyncIdleTime());
			exceptionGenerator.produceRandomRuntimeException();
			logger.info("Async method was successfull");
		} catch (InterruptedException e) {
			logger.error(e.getMessage());

			// Restore interrupted state...
			Thread.currentThread().interrupt();
		} catch (BaseException e) {
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * Call done resource under configured endpoint 
	 * @return String obtained by get endpoint
	 */
	@Override
	public String getDone() {
		String url = baseUrl + DONE_ENDPOINT_URI;
		String results = null;
		
		try
		{
			logger.info("Calling get on endpoint '{}'",url);
			results = restTemplate.getForObject(url, String.class);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return results;
    }

	/**
	 * @param ex Throwable exception
	 * @return boolean specifying if it was posted successfully
	 */
	@Override
	public boolean postError(Throwable ex) {
		String url = baseUrl + ERROR_ENDPOINT_URI;
		boolean success = false;
		String response = null;
		try {
			ErrorResponse error = ErrorResponse.builder()
					.error(ex.getMessage())
					.timestamp(System.currentTimeMillis())
					.build();
			
			String errorStr = mapper.writeValueAsString(error);
			
			logger.info("Calling post on endpoint '{}' with body {}",url,errorStr);
			
			response = restTemplate.postForObject(url, error, String.class);
			
			return response.equals(POST_ERROR_SUCCESS_MESSAGE);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return success;
		
	}

}
