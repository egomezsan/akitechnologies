package com.aki.technologies.service;

import org.springframework.web.client.RestTemplate;

/**
 * Service interface
 * @author eduardo.gomez
 *
 */
public interface IAkiService {
	
	public void produceRandomRuntimeException();
	public void produceRandomRuntimeExceptionAsync();
	public String getDone();
	public boolean postError(Throwable ex);
	public void setRestTemplate(RestTemplate restTemplate);

}
