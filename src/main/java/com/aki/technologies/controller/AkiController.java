package com.aki.technologies.controller;

import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.aki.technologies.exception.BaseException;
import com.aki.technologies.model.AkiRequest;
import com.aki.technologies.model.AkiResponse;
import com.aki.technologies.model.ErrorResponse;
import com.aki.technologies.service.IAkiService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import static com.aki.technologies.util.ServiceConstants.*;

/**
 * Aki Rest Controller class which exposes 2 endpoints
 * @author eduardo.gomez
 *
 */
@AllArgsConstructor
@RestController
public class AkiController {
	
	private IAkiService service;
	
	/**
	 * A simple health check endpoint
	 * @return ResponseEntity.ok()
	 */
	@GetMapping(HEALTH_CHECK_MAPPING)
	public ResponseEntity<String> getStatus()
	{
		return ResponseEntity.ok()
				.build();
	}
	
	/**
	 * Main endpoint which produces runtimeExceptions randomly, first sync and then async
	 * @param request
	 * @return AkiResponse
	 */
	@PostMapping(SERVICE_MAPPING)
	public AkiResponse callService(@Valid @RequestBody AkiRequest request)
	{
		service.produceRandomRuntimeException();
		service.produceRandomRuntimeExceptionAsync();

		return new AkiResponse(SUCCESS_RESPONSE_MESSAGE);
	}
	
	/**
	 * Exception handler for this controller which handle the MethodArgumentNotValidException which is 
	 * thrown when validation fails
	 * @param ex MethodArgumentNotValidException
	 * @return ErrorResponse
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        return ErrorResponse.builder()
        		.error(result.getGlobalError().getDefaultMessage())
        		.build();

    }
	
	/**
	 * Main exception handler which handles BaseException
	 * @param exception BaseException
	 * @return ErrorResponse
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorResponse handleGenericException(BaseException exception)
	{
		return ErrorResponse.builder()
        		.error(ERROR_RESPONSE_MESSAGE)
        		.build();
	}

}
