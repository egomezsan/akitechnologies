package com.aki.technologies.validation;

import java.lang.reflect.Field;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Performs option value validation rules:
 * 
 * {"op": "A", values: [int, ..., ..]} // any number of ints
 * {"op": "B", values: [int, int]|[int]} // when there's two values, the first int must be larger than the second
 * {"op": "C", values: [int]} // single int
 * 
 * @author eduardo.gomez
 *
 */
public class OptionValuesValidator implements ConstraintValidator<OptionValuesConstraint, Object>{
	
	private String optionField;
    private String valuesField;
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Override
    public void initialize(OptionValuesConstraint constraint) {
    	optionField = constraint.optionField();
    	valuesField = constraint.valuesField();
    }

    /**
     * Performs validation
     */
	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		
        try {
        	String optionFieldValue =  getFieldValue(object, optionField).toString();
        	
        	@SuppressWarnings("unchecked")
        	List<Integer>  valuesFieldValue = (List<Integer>) getFieldValue(object, valuesField);
			
			if(null == valuesFieldValue || null == optionFieldValue)
				return false;
			
			switch (optionFieldValue) {
			case "A":
				return (!valuesFieldValue.isEmpty());
			case "B":
				return (valuesFieldValue.size() == 1 || (valuesFieldValue.size() == 2 && valuesFieldValue.get(0) > valuesFieldValue.get(1)));
			case "C":
				return (valuesFieldValue.size() == 1);
			default:
				return false;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return false;
		}
	}
	
	/**
	 * Get field value from generic object
	 * 
	 * @param object
	 * @param fieldName
	 * @return Object containing value of such field
	 * @throws Exception
	 */
	private Object getFieldValue(Object object, String fieldName) throws Exception {
        Class<?> clazz = object.getClass();
        Field passwordField = clazz.getDeclaredField(fieldName);
        passwordField.setAccessible(true);
        return passwordField.get(object);
    }

}
