package com.aki.technologies.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import static com.aki.technologies.util.ServiceConstants.*;

/**
 * Constraint class annotation used for validating option values rules.
 * 
 * @author eduardo.gomez
 *
 */
@Documented
@Constraint(validatedBy = OptionValuesValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OptionValuesConstraint {
	
	String message() default VALIDATION_ERROR_MESSAGE;
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
	String optionField();
	 
    String valuesField();

}
