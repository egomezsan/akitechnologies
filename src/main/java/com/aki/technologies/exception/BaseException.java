package com.aki.technologies.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base exception
 * @author eduardo.gomez
 *
 */
public class BaseException extends RuntimeException{
	
	private final transient Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final long serialVersionUID = 5399810057543983590L;
	
	public BaseException( String errorMessage) {
		super (errorMessage);
	}
	
	public BaseException(Exception ex) {
		super(ex);
		logger.error(ex.getMessage());
	}

	public BaseException() {
		super();
	}

}
