package com.aki.tecnologies;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import java.util.Arrays;
import java.util.Random;
import static com.aki.technologies.util.ServiceConstants.*;
import com.aki.technologies.AkiTechnologiesProjectApplication;
import com.aki.technologies.configuration.AkiSettings;
import com.aki.technologies.model.AkiRequest;
import com.aki.technologies.service.IAkiService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration test for PostExecutionResponseHandler aspect
 * 
 * @author eduardo.gomez
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AkiTechnologiesProjectApplication.class)
public class PostExecutionResponseHandlerIT {

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private IAkiService service;

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;

	private MockRestServiceServer server;

	@Autowired
	AkiSettings settings;

	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private Random random;

	private String baseUrl;

	/**
	 * Setup web context & MockRestServiceServer
	 * 
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception {

		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
		MockitoAnnotations.initMocks(this);
		RestTemplate restTemplate = restTemplateBuilder.build();
		baseUrl = String.format(URL_STRING_FORMAT, settings.getScheme(), settings.getHost(), settings.getPort());
		server = MockRestServiceServer.createServer(restTemplate);
		service.setRestTemplate(restTemplate);

	}

	/**
	 * Success test scenario
	 * 
	 * @throws Exception
	 */
	@Test
	public void callServiceWithSuccessResponse() throws Exception {

		AkiRequest request = AkiRequest.builder().option("A").values(Arrays.asList(1, 2, 3, 4, 5)).build();

		doReturn(false).when(random).nextBoolean();

		// Validate that done endpoint with get method is called on success
		this.server.expect(ExpectedCount.manyTimes(), requestTo(baseUrl + DONE_ENDPOINT_URI)).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess("Success", MediaType.TEXT_PLAIN));

		mockMvc.perform(post(SERVICE_MAPPING).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request)));

		server.verify();

	}

	/**
	 * Test error scenario
	 * 
	 * @throws Exception
	 */
	@Test
	public void callServiceWithErrorResponse() throws Exception {

		AkiRequest request = AkiRequest.builder().option("A").values(Arrays.asList(1, 2, 3, 4, 5)).build();

		doReturn(true).when(random).nextBoolean();

		// Validate that error endpoint
		this.server.expect(ExpectedCount.manyTimes(),requestTo(baseUrl + ERROR_ENDPOINT_URI)).andExpect(method(HttpMethod.POST))
				.andExpect(jsonPath("$.error").value(RANDOM_EXCEPTION_MESSAGE))
				.andRespond(withSuccess(POST_ERROR_SUCCESS_MESSAGE, MediaType.TEXT_PLAIN));

		mockMvc.perform(post(SERVICE_MAPPING).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request)));

	}

}
