package com.aki.tecnologies.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import com.aki.technologies.model.AkiRequest;

/**
 * Test class to unit test custom validator
 * @author eduardo.gomez
 *
 */
public class OptionValuesValidatorTest {
	
	private static Validator validator;
	private static Set<ConstraintViolation<AkiRequest>> violations;
	
	/**
	 * Setup validator
	 */
	@Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        violations = null;
    }
	
	/**
	 * Test All possible scenarios for Option A
	 */
	@Test
	public void isValidTestOptionA()
	{
		// Initialize request with 3 values (valid)
		AkiRequest request = AkiRequest.builder()
				.option("A")
				.values(Arrays.asList(1,2,3))
				.build();
		
		validScenario(request);
        
        // Now test with only one value (valid)
        request.setValues(Arrays.asList(1));
        validScenario(request);
        
        // Now test with no values (invalid)
        request.setValues(new ArrayList<>());
        invalidScenario(request);
        
        // Now test with null (invalid)
        request.setValues(null);
        invalidScenario(request);
        
	}
	
	/**
	 * Test All possible scenarios for Option B
	 */
	@Test
	public void isValidTestOptionB()
	{
		// Initialize request with 2 values and first > second (valid)
		AkiRequest request = AkiRequest.builder()
				.option("B")
				.values(Arrays.asList(2,1))
				.build();
		
		validScenario(request);
        
        // Now test with only one value (valid)
        request.setValues(Arrays.asList(1));
        validScenario(request);
        
        // Now test with no values (invalid)
        request.setValues(new ArrayList<>());
        invalidScenario(request);
        
        // Now test with null (invalid)
        request.setValues(null);
        invalidScenario(request);
        
        // Now test with > 2 values (invalid)
        request.setValues(Arrays.asList(1,2,3));
        invalidScenario(request);
        
        // Now test with 2 values and first == second (invalid)
        request.setValues(Arrays.asList(1,1));
        invalidScenario(request);
        
        // Now test with 2 values and first < second (invalid)
        request.setValues(Arrays.asList(1,2));
        invalidScenario(request);
        
	}
	
	/**
	 * Test All possible scenarios for Option C
	 */
	@Test
	public void isValidTestOptionC()
	{
		// Initialize request with one value (valid)
		AkiRequest request = AkiRequest.builder()
				.option("B")
				.values(Arrays.asList(2))
				.build();
		
		validScenario(request);
        
        // Now test with > one value (invalid)
        request.setValues(Arrays.asList(1,2));
        invalidScenario(request);
        
        // Now test with no values (invalid)
        request.setValues(new ArrayList<>());
        invalidScenario(request);
        
        // Now test with null (invalid)
        request.setValues(null);
        invalidScenario(request);    
	}
	
	/**
	 * Utility method for validating valid scenarios
	 * @param request AkiRequest to be validated
	 */
	private void validScenario(AkiRequest request)
	{
		violations = validator.validate(request);
        assertTrue(violations.isEmpty());
	}
	
	/**
	 * Utility method for validating invalid scenarios
	 * @param request AkiRequest to be validated
	 */
	private void invalidScenario(AkiRequest request)
	{
		violations = validator.validate(request);
        assertFalse(violations.isEmpty());
	}

}
