package com.aki.tecnologies;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import static com.aki.technologies.util.ServiceConstants.*;
import com.aki.technologies.AkiTechnologiesProjectApplication;
import com.aki.technologies.configuration.AkiSettings;
import com.aki.technologies.controller.AkiController;
import com.aki.technologies.model.AkiRequest;
import com.aki.technologies.service.IAkiService;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.awaitility.Awaitility.await;

/**
 * Integration test for /service endpoint
 * @author eduardo.gomez
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes= AkiTechnologiesProjectApplication.class)
public class ServiceEndpointIT {
	
	private MockRestServiceServer server;
	
	@Autowired
	private IAkiService service;

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	
	private MockMvc mockMvc;
	
	@Autowired
    private AkiController controller;
	
	@Autowired
    private ObjectMapper mapper;
	
	@Autowired
    private AkiSettings settings;
	
	@MockBean
    private Random random;
	
	private String baseUrl;
	
	/**
	 * Setup standalone context
	 * @throws Exception
	 */
	@Before
    public void setup() throws Exception {
        this.mockMvc = standaloneSetup(this.controller).build();
        RestTemplate restTemplate = restTemplateBuilder.build();
		baseUrl = String.format(URL_STRING_FORMAT, settings.getScheme(), settings.getHost(), settings.getPort());
		server = MockRestServiceServer.createServer(restTemplate);
		service.setRestTemplate(restTemplate);
		
    }
	
	/**
	 *  Success test scenario
	 * @throws Exception
	 */
	@Test
    public void callServiceWithSuccessResponse() throws Exception {
		
		AkiRequest request = AkiRequest.builder()
				.option("A")
				.values(Arrays.asList(1,2,3,4,5))
				.build();
		
		doReturn(false)
        .when(random)
        .nextBoolean();
		
		this.server.expect(ExpectedCount.manyTimes(), requestTo(baseUrl + DONE_ENDPOINT_URI))
		.andRespond(withSuccess("Success", MediaType.TEXT_PLAIN));
		
        mockMvc.perform(post(SERVICE_MAPPING).contentType(MediaType.APPLICATION_JSON)
        		.content(mapper.writeValueAsString(request)))
        	.andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(SUCCESS_RESPONSE_MESSAGE));
        
        // Verify that async method is invoked (meaning random.nextBoolean is executed twice)        
        await().atMost(settings.getAsyncIdleTime()+5000, TimeUnit.MILLISECONDS).untilAsserted(() -> 
        verify(random, times(2)).nextBoolean());
        
    }
	
	/**
	 * Test error response
	 * @throws Exception
	 */
	@Test
    public void callServiceWithErrorResponse() throws Exception {
		
		AkiRequest request = AkiRequest.builder()
				.option("A")
				.values(Arrays.asList(1,2,3,4,5))
				.build();
		
		doReturn(true)
        .when(random)
        .nextBoolean();
		
		this.server.expect(ExpectedCount.manyTimes(),requestTo(baseUrl + ERROR_ENDPOINT_URI))
		.andRespond(withSuccess(POST_ERROR_SUCCESS_MESSAGE, MediaType.TEXT_PLAIN));
		
        mockMvc.perform(post(SERVICE_MAPPING).contentType(MediaType.APPLICATION_JSON)
        		.content(mapper.writeValueAsString(request)))
        	.andDo(print())
            .andExpect(status().isInternalServerError())
            .andExpect(jsonPath("$.error").value(ERROR_RESPONSE_MESSAGE));
        
        // Verify that async method is not invoked       
        await().atMost(settings.getAsyncIdleTime()+5000, TimeUnit.MILLISECONDS).untilAsserted(() -> 
        verify(random, times(1)).nextBoolean());
    }
	
	/**
	 * Test Invalid Request
	 * @throws Exception
	 */
	@Test
    public void callServiceWithInvalidRequest() throws Exception {
		
		AkiRequest request = AkiRequest.builder()
				.option("C")
				.values(Arrays.asList(1,2,3,4,5))
				.build();
		
        mockMvc.perform(post(SERVICE_MAPPING).contentType(MediaType.APPLICATION_JSON)
        		.content(mapper.writeValueAsString(request)))
        	.andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.error").value(VALIDATION_ERROR_MESSAGE));
    }

}
